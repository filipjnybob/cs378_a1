Pawn common behavior:
To move the pawn :
	 W : go forward,  
	 S : go backward,  
	 A : move left, 
	 D : move right
	 F : Switch to another pawn

Behavior1_Pawn

To switch control between Behavior1Panw and Behavior2_Pawn : F

For doing switch possession between 2 pawns, we create a function named
 PerformSwitch() and calls it whenever player hits F key. 
Regarding the implementation, we get an array of actor in the world, then looks
for an instance of the other type pawn (in the case of this assignement, there
is only one instance of the other type pawn). When we find an instance of other
type pawn, we call GetController() on the current pawn to get a reference to
the controller. Then calls UnPossess() on this controller so that it is no
longer control the current pawn. Finally, we pass the pawn we just find as an
argument for the function Possess() which called on the controller.


Behavior2_Pawn:
In the Tick function, we check a collision with interactable objects and if
collided actor is an interactable objects then we addImpulse() on it to forward
direction.

Feature1InteractableActor:
We set Trigger Component(USphere Component) as a root component.
It freely move around the map, but when it collide with the wall, 
it will be reflected to other direction by using FMath::GetReflectionVector().


PendulumActor


We created a root component as the base component. 
The StableComponent and BounceComponents are the stable rod and the swinging
ball parts of the system, respectively. 
The constraint component ties both the StableComponent and BounceComponents
together via the SetConstrainedComponents() function in BeginPlay(). 
The constraint variables are also set via the SetConstraintVariables() function. 
The rope is added via the UCableComponent and attached in Blueprint.


Interactable3: Teleporting object

TeleLocActor:

This actor is a hidden-in-game actor that acts as a “teleporting destination”,
where TeleObjectActors teleport to.
It consists of a simple MeshComponent. Its sole purpose is for the assurance
of teleportations to not go out of bound. 

TeleObjectActor:

This actor consists of a StaticMeshComponent and TriggerComponent. 
TeleObjectActor keeps two pointers to TeleLocActor instances. 
(This can potentially be expanded to an array of more than 2 teleportation
destinations but since we have 3 teleporting interactables zooming around 
6 destinations total, we felt that this was a bit overwhelming.) TeleLocActor
pointers are added to TeleObjectActor as a UPROPERTY for every instance of
TeleObjectActor. A repeating timer is set (MemberTimerHandle) in BeginPlay(),
which fires every 8 seconds. The timer fires the Timer Delegate TimerDel,
banded to a Lambda. Within the Lambda, SetNextLocation() is called.
SetNextLocation just sets the teleport destination of the object to the
“other” TeleLocActor destination that it did not teleport to previously.
TeleObjectActor’s location is then set to this TeleLocActor destination.

Kill Zone:
This actor is only comprised of a HitBoxComponent. When the pawn overlaps with
the hitbox, SetActorLocation() is called to reset the pawn to an FVector equal
to that of the player start.