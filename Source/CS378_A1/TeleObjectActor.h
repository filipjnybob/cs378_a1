// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameFramework/SpringArmComponent.h"
#include "TeleLocActor.h"
#include "TeleObjectActor.generated.h"

UCLASS()
class CS378_A1_API ATeleObjectActor : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ATeleObjectActor();

	FTimerHandle MemberTimerHandle;
	FTimerDelegate TimerDel;
	FORCEINLINE class UStaticMeshComponent *GetMeshComponent() const { return MeshComponent; }
	FORCEINLINE class USphereComponent *GetTriggerComponent() const { return TriggerComponent; }

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(Category = Mesh, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent *MeshComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USphereComponent *TriggerComponent;

	UFUNCTION()
	void SetNextLocation();

	UPROPERTY(EditAnywhere, Category = "Teleport Location")
	ATeleLocActor *location1;

	UPROPERTY(EditAnywhere, Category = "Teleport Location")
	ATeleLocActor *location2;

	ATeleLocActor *curLocation;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
