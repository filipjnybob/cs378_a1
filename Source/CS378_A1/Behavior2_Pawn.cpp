// Fill out your copyright notice in the Description page of Project Settings.

#include "Behavior2_Pawn.h"
#include "Components/BoxComponent.h"
#include "Feature1InteractableActor.h"
#include "Components/SphereComponent.h"
#include "PendulumActor.h"
#include "TeleObjectActor.h"
#include "Kismet/GameplayStatics.h"
#include "Behavior1Pawn.h"

const FName ABehavior2_Pawn::MoveForwardBinding("MoveForward");
const FName ABehavior2_Pawn::MoveRightBinding("MoveRight");
const FName ABehavior2_Pawn::SwitchControllerBinding("SwitchController");
// Sets default values
ABehavior2_Pawn::ABehavior2_Pawn()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	RootComponent = MeshComponent;

	HitboxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("HitboxComponent"));
	HitboxComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	MoveSpeed = 1000.0f;
}

// Called when the game starts or when spawned
void ABehavior2_Pawn::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(0.0f, 0.0f, 0.0f));
}

// Called every frame
void ABehavior2_Pawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	SetActorRotation(FRotator(0.0f, 0.0f, 0.0f));

	const float ForwardValue = GetInputAxisValue(MoveForwardBinding);
	const float RightValue = GetInputAxisValue(MoveRightBinding);
	const float SwitchControllerValue = GetInputAxisValue(SwitchControllerBinding);

	// Clamp max size so that (X=1, Y=1) doesn't cause faster movement in diagonal directions
	const FVector MoveDirection = FVector(ForwardValue, RightValue, 0.f).GetClampedToMaxSize(1.0f);

	// Calculate  movement
	const FVector Movement = MoveDirection * MoveSpeed * DeltaTime;

	// If non-zero size, move this actor
	if (Movement.SizeSquared() > 0.0f)
	{
		const FRotator NewRotation = Movement.Rotation();
		FHitResult Hit(1.f);
		RootComponent->MoveComponent(Movement, NewRotation, true, &Hit);

		if (Hit.IsValidBlockingHit())
		{
			const FVector Normal2D = Hit.Normal.GetSafeNormal2D();
			const FVector Deflection = FVector::VectorPlaneProject(Movement, Normal2D) * (1.f - Hit.Time);
			RootComponent->MoveComponent(Deflection, NewRotation, true);
		}
	}

	TArray<AActor *> OverlappingActors;
	HitboxComponent->GetOverlappingActors(OverlappingActors);
	for (AActor *actor : OverlappingActors)
	{

		if (actor->IsA(AFeature1InteractableActor::StaticClass()) ||
			actor->IsA(ATeleObjectActor::StaticClass()))
		{
			USphereComponent *Feature1InteractableActor = Cast<USphereComponent>(actor->GetRootComponent());
			Feature1InteractableActor->AddImpulse(GetActorForwardVector() * 100.0f * Feature1InteractableActor->GetMass());
		}
	}
}

void ABehavior2_Pawn::PerformSwitch()
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Switch"));

	TArray<AActor *> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ABehavior1Pawn::StaticClass(), FoundActors);

	for (AActor *actor : FoundActors)
	{
		if (actor->IsA(ABehavior1Pawn::StaticClass()))
		{
			//Cast<APlayerController>(GetController());
			APlayerController *controller = Cast<APlayerController>(GetController());
			controller->UnPossess();

			APawn *pawn = Cast<APawn>(actor);
			controller->Possess(pawn);
		}
	}
}

// Called to bind functionality to input
void ABehavior2_Pawn::SetupPlayerInputComponent(UInputComponent *PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(MoveForwardBinding);
	PlayerInputComponent->BindAxis(MoveRightBinding);
	PlayerInputComponent->BindAction(SwitchControllerBinding, IE_Pressed, this, &ABehavior2_Pawn::SwitchPressed);
}
