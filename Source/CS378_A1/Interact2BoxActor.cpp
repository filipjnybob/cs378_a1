// Fill out your copyright notice in the Description page of Project Settings.

#include "Interact2BoxActor.h"
#include "Components/BoxComponent.h"
#include "WallActor.h"
#include "Math/UnrealMathUtility.h"

// Sets default values
AInteract2BoxActor::AInteract2BoxActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	TriggerComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("TriggerComponent"));
	RootComponent = TriggerComponent;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	HitboxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("HitboxComponent"));
	HitboxComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
}

// Called when the game starts or when spawned
void AInteract2BoxActor::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AInteract2BoxActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	SetActorLocation(GetActorLocation() + GetActorForwardVector() * 3.0f);

	TArray<AActor *> OverlappingActors;
	HitboxComponent->GetOverlappingActors(OverlappingActors);
	for (AActor *actor : OverlappingActors)
	{
		if (actor->IsA(AWallActor::StaticClass()))
		{

			AWallActor *WallActor = Cast<AWallActor>(actor);
			FVector newVector = FMath::GetReflectionVector(GetActorLocation(), WallActor->GetActorLocation());

			int yAxis = FMath::FRandRange(0, 45);
			SetActorRotation(newVector.Rotation() + FRotator(0.0f, yAxis, 0.0f));

			SetActorLocation(GetActorLocation() + GetActorForwardVector() * 3.0f);
		}
	}
}
