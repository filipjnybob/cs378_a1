// Fill out your copyright notice in the Description page of Project Settings.

#include "KillZoneActor.h"
#include "Components/BoxComponent.h"
#include "Behavior1Pawn.h"
#include "Behavior2_Pawn.h"

// Sets default values
AKillZoneActor::AKillZoneActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	HitboxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("HitboxComponent"));
	RootComponent = HitboxComponent;
}

// Called when the game starts or when spawned
void AKillZoneActor::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AKillZoneActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TArray<AActor *> OverlappingActors;
	HitboxComponent->GetOverlappingActors(OverlappingActors);
	for (AActor *actor : OverlappingActors)
	{
		if (actor->IsA(ABehavior1Pawn::StaticClass()) || actor->IsA(ABehavior2_Pawn::StaticClass()))
		{
			actor->SetActorLocation(FVector(0.0f, 0.0f, 112.0f));
		}
	}
}
