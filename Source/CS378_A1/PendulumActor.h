// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PhysicsEngine/PhysicsConstraintComponent.h"
#include "CableComponent.h"
#include "PendulumActor.generated.h"

UCLASS()
class CS378_A1_API APendulumActor : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	APendulumActor();

	FORCEINLINE class UStaticMeshComponent *GetStableComponent() const { return StableComponent; }
	FORCEINLINE class UStaticMeshComponent *GetBounceComponent() const { return BounceComponent; }
	FORCEINLINE class USphereComponent *GetTriggerComponent() const { return TriggerComponent; }

	FORCEINLINE class UPhysicsConstraintComponent *GetPhysicsConstraintComponent() const { return ConstraintComponent; }

	// FORCEINLINE class USceneComponent *GetPhysicsConstraintComponent() const { return ConstraintComponent; }

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(Category = Mesh, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent *StableComponent;

	UPROPERTY(Category = Mesh, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent *BounceComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UPhysicsConstraintComponent *ConstraintComponent;

	UPROPERTY(Category = Cable, VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	class UCableComponent *Cable;

	UFUNCTION()
	void SetConstraintVariables();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USphereComponent *TriggerComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USphereComponent *SceneComp;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
