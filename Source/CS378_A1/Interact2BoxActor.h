// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interact2BoxActor.generated.h"

UCLASS()
class CS378_A1_API AInteract2BoxActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AInteract2BoxActor();
	FORCEINLINE class UStaticMeshComponent* GetMeshComponent() const { return MeshComponent; }
	FORCEINLINE class UBoxComponent* GetTriggerComponent() const { return TriggerComponent; }
	FORCEINLINE class UBoxComponent* GetHitboxComponent() const { return HitboxComponent; }

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UPROPERTY(Category = Mesh, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class UStaticMeshComponent* MeshComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UBoxComponent* TriggerComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UBoxComponent* HitboxComponent;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
