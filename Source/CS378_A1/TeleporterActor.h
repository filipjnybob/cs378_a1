// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Behavior1Pawn.h"
#include "Behavior2_Pawn.h"
#include "GameFramework/SpringArmComponent.h"
#include "TeleporterActor.generated.h"

UCLASS()
class CS378_A1_API ATeleporterActor : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ATeleporterActor();

	FTimerHandle MemberTimerHandle;

	FORCEINLINE class UStaticMeshComponent *GetMeshComponent() const { return MeshComponent; }
	FORCEINLINE class USphereComponent *GetTriggerComponent() const { return TriggerComponent; }

	UPROPERTY(EditAnywhere, Category = "Teleporter")
	ATeleporterActor *otherTele;

	UPROPERTY()
	bool isTeleporting;

	UPROPERTY()
	bool alreadySteppedOff;

	UFUNCTION()
	void StepInTeleporter(class AActor *overlappedActor, class AActor *otherActor);

	UFUNCTION()
	void StepOutTeleporter(class AActor *overlappedActor, class AActor *otherActor);

	UFUNCTION()
	void SetIsTeleporting();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/* The mesh component */
	UPROPERTY(Category = Mesh, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent *MeshComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USphereComponent *TriggerComponent;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
