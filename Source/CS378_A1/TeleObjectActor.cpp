// Fill out your copyright notice in the Description page of Project Settings.

#include "TeleObjectActor.h"
#include "Components/SphereComponent.h"

// Sets default values
ATeleObjectActor::ATeleObjectActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	TriggerComponent = CreateDefaultSubobject<USphereComponent>(TEXT("TriggerComponent"));
	RootComponent = TriggerComponent;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
}

// Called when the game starts or when spawned
void ATeleObjectActor::BeginPlay()
{
	Super::BeginPlay();

	curLocation = location1;

	if (curLocation && location1 && location2)
	{
		TimerDel.BindLambda([&]()
							{
								SetNextLocation();

								SetActorRotation(curLocation->GetActorRotation());
								SetActorLocation(curLocation->GetActorLocation());
							});

		GetWorldTimerManager().SetTimer(MemberTimerHandle, TimerDel, 8.0f, true, 0.5f);
	}
}

// Called every frame
void ATeleObjectActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ATeleObjectActor::SetNextLocation()
{
	if (curLocation->GetName() == location1->GetName())
	{
		curLocation = location2;
	}
	else
	{
		curLocation = location1;
	}
}
