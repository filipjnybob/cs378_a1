// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "KillZoneActor.generated.h"

UCLASS()
class CS378_A1_API AKillZoneActor : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AKillZoneActor();
	FORCEINLINE class UBoxComponent *GetHitboxComponent() const { return HitboxComponent; }

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UBoxComponent *HitboxComponent;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
