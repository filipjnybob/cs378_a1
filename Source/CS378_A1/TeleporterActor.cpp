// Fill out your copyright notice in the Description page of Project Settings.

#include "TeleporterActor.h"
#include "Components/SphereComponent.h"
#include "Behavior1Pawn.h"
#include "Behavior2_Pawn.h"

// Sets default values
ATeleporterActor::ATeleporterActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	RootComponent = MeshComponent;

	TriggerComponent = CreateDefaultSubobject<USphereComponent>(TEXT("TriggerComponent"));
	TriggerComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	isTeleporting = 0;
	alreadySteppedOff = 0;

	OnActorBeginOverlap.AddDynamic(this, &ATeleporterActor::StepInTeleporter);
	OnActorBeginOverlap.AddDynamic(this, &ATeleporterActor::StepOutTeleporter);
}

// Called when the game starts or when spawned
void ATeleporterActor::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ATeleporterActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ATeleporterActor::StepInTeleporter(class AActor *overlappedActor, class AActor *otherActor)
{
	if (otherActor && otherActor != this && otherTele)
	{

		if (otherActor->IsA(ABehavior1Pawn::StaticClass()))
		{
			if (otherActor && !isTeleporting && !otherTele->isTeleporting)
			{
				isTeleporting = true;
				ABehavior1Pawn *pawn = Cast<ABehavior1Pawn>(otherActor);

				pawn->SetActorRotation(otherTele->GetActorRotation());
				pawn->SetActorLocation(otherTele->GetActorLocation() + FVector(0, 0, 75));
			}
		}
		else if (otherActor->IsA(ABehavior2_Pawn::StaticClass()))
		{
			if (otherActor && !isTeleporting && !otherTele->isTeleporting)
			{
				isTeleporting = true;
				ABehavior2_Pawn *pawn = Cast<ABehavior2_Pawn>(otherActor);

				pawn->SetActorRotation(otherTele->GetActorRotation());
				pawn->SetActorLocation(otherTele->GetActorLocation() + FVector(0, 0, 75));
			}
		}
	}
}

void ATeleporterActor::StepOutTeleporter(class AActor *overlappedActor, class AActor *otherActor)
{
	if (otherActor && otherActor != this)
	{
		if (otherTele && !isTeleporting && !alreadySteppedOff)
		{
			alreadySteppedOff = true;
			GetWorldTimerManager().SetTimer(MemberTimerHandle, this, &ATeleporterActor::SetIsTeleporting, 0.1f, false, 7.0f);
		}
	}
}

void ATeleporterActor::SetIsTeleporting()
{
	otherTele->isTeleporting = false;
	alreadySteppedOff = false;
}
