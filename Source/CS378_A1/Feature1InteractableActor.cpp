// Fill out your copyright notice in the Description page of Project Settings.

#include "Feature1InteractableActor.h"
#include "Components/BoxComponent.h"
#include "Components/SphereComponent.h"
#include "WallActor.h"
#include "Math/UnrealMathUtility.h"

// Sets default values
AFeature1InteractableActor::AFeature1InteractableActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	TriggerComponent = CreateDefaultSubobject<USphereComponent>(TEXT("TriggerComponent"));
	RootComponent = TriggerComponent;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	HitboxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("HitboxComponent"));
	HitboxComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
}

// Called when the game starts or when spawned
void AFeature1InteractableActor::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AFeature1InteractableActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	SetActorLocation(GetActorLocation() + GetActorForwardVector() * 3.0f);

	TArray<AActor *> OverlappingActors;
	HitboxComponent->GetOverlappingActors(OverlappingActors);
	for (AActor *actor : OverlappingActors)
	{
		if (actor->IsA(AWallActor::StaticClass()))
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("wall Collide!!"));

			AWallActor *WallActor = Cast<AWallActor>(actor);
			FVector newVector = FMath::GetReflectionVector(GetActorLocation(), WallActor->GetActorLocation());

			int yAxis = FMath::FRandRange(0, 45);
			SetActorRotation(newVector.Rotation() + FRotator(0.0f, yAxis, 0.0f));

			SetActorLocation(GetActorLocation() + GetActorForwardVector() * 3.0f);
		}
	}
}
