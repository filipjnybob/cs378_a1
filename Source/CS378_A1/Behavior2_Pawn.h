// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Behavior2_Pawn.generated.h"

UCLASS()
class CS378_A1_API ABehavior2_Pawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ABehavior2_Pawn();
	FORCEINLINE class UStaticMeshComponent *GetMeshComponent() const { return MeshComponent; }
	FORCEINLINE class UBoxComponent *GetHitboxComponent() const { return HitboxComponent; }

	static const FName MoveForwardBinding;
	static const FName MoveRightBinding;
	static const FName SwitchControllerBinding;

	UPROPERTY(Category = Gameplay, EditAnywhere, BlueprintReadWrite)
	float MoveSpeed;

	UFUNCTION(BlueprintImplementableEvent)
	void SwitchPressed();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(Category = Mesh, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent *MeshComponent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UBoxComponent *HitboxComponent;

	UFUNCTION(BlueprintCallable)
	void PerformSwitch();

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent *PlayerInputComponent) override;
};
