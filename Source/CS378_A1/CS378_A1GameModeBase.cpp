#include "CS378_A1GameModeBase.h"
#include "Behavior2_Pawn.h"
#include "Behavior1Pawn.h"

ACS378_A1GameModeBase::ACS378_A1GameModeBase()
{
    static ConstructorHelpers::FObjectFinder<UClass> pawnBPClass(TEXT("Class'/Game/Blueprint/Behavior1PawnBP.Behavior1PawnBP_C'"));
    if (pawnBPClass.Object)
    {
        UClass *pawnBP = (UClass *)pawnBPClass.Object;
        DefaultPawnClass = pawnBP;
    }
    else
    {
        DefaultPawnClass = ABehavior2_Pawn::StaticClass();
    }
}