// Fill out your copyright notice in the Description page of Project Settings.

#include "PendulumActor.h"
#include "Components/SphereComponent.h"

// Sets default values
APendulumActor::APendulumActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

	ConstraintComponent = CreateDefaultSubobject<UPhysicsConstraintComponent>(TEXT("ConstraintComponent"));
	ConstraintComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	StableComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StableComponent"));
	StableComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	TriggerComponent = CreateDefaultSubobject<USphereComponent>(TEXT("TriggerComponent"));
	TriggerComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	BounceComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BounceComponent"));
	BounceComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	Cable = CreateDefaultSubobject<UCableComponent>(TEXT("Cable"));
	Cable->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepWorldTransform);
}

// Called when the game starts or when spawned
void APendulumActor::BeginPlay()
{
	Super::BeginPlay();

	SetConstraintVariables();

	ConstraintComponent->SetConstrainedComponents(StableComponent, "Stable Component", BounceComponent, "Bounce Component");
}

// Called every frame
void APendulumActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void APendulumActor::SetConstraintVariables()
{
	FConstraintInstance platformConstraintInstance;
	FConstraintProfileProperties platformConstraintProperties = platformConstraintInstance.ProfileInstance;
	platformConstraintInstance.SetLinearXMotion(ELinearConstraintMotion::LCM_Locked);
	platformConstraintInstance.SetLinearYMotion(ELinearConstraintMotion::LCM_Locked);
	platformConstraintInstance.SetLinearZMotion(ELinearConstraintMotion::LCM_Locked);
	platformConstraintInstance.ProfileInstance.LinearLimit.Limit = 5.0;
	platformConstraintInstance.ProfileInstance.LinearLimit.bSoftConstraint = true;
	platformConstraintInstance.ProfileInstance.LinearLimit.Stiffness = 3000.0;
	platformConstraintInstance.ProfileInstance.LinearLimit.Restitution = 1.0;
	platformConstraintInstance.ProfileInstance.LinearLimit.ContactDistance = 1.0;
	platformConstraintInstance.SetAngularSwing1Limit(EAngularConstraintMotion::ACM_Free, 0);
	platformConstraintInstance.SetAngularSwing2Limit(EAngularConstraintMotion::ACM_Free, 3.0);
	platformConstraintInstance.SetAngularTwistLimit(EAngularConstraintMotion::ACM_Free, 0);
	platformConstraintInstance.ProfileInstance.ConeLimit.Stiffness = 1.0;
	platformConstraintInstance.ProfileInstance.ConeLimit.Restitution = 1.0;

	ConstraintComponent->ConstraintInstance = platformConstraintInstance;
}
